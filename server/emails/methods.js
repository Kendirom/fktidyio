Meteor.methods({
	'sendTestEmail': function(to){
		this.unblock();
	    Email.send({
	      to: to,
	      from: 'luler.com',
	      subject: 'Test email',
	      text: 'Test email'
	    });
	},

	'sendInvintation': function(token){
		if(!this.userId){
			throw new Meteor.Error('123', 'Not logged in');
		}
		var doc = Tokens.findOne({_id: token});
		if(doc){
			this.unblock();
		    Email.send({
		      to: doc.email,
		      from: 'Tidyio team',
		      subject: 'Register invintation',
		      text: 'Hi! Join Tidyio team! ' + Meteor.absoluteUrl() + 'register/' + token
		    });
		} else {
			throw new Meteor.Error('404', 'Token not found');
		}
		
	}
})