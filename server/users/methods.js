Meteor.methods({
	'users.insert' : function(email, password, profile){
		// check({
		// 	email : String,
		// 	password : String,
		// });

		var userId = Accounts.createUser({
			email : email,
			password : password,
			profile : profile
		});

		Roles.addUsersToRoles(userId, ['without-role']);
		return userId;
	},

	'users.setRole' : function(userId, role){
		Roles.setUserRoles(userId, [role]);
		return userId;
	},

	'users.update': function(profile){
		var userId = this.userId;
		Meteor.users.update({_id: userId}, {$set:{profile: profile, 'emails.0.address': profile.email}});
	}
});
