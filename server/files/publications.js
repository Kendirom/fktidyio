Meteor.publish('logo', function(id){
	return Logos.find({'metadata.companyId': id})
});

Meteor.publish('pdf', function(id){
	return PdfTarifs.find({'metadata.companyId': id})
});

Meteor.publish('exel', function(id){
	return ExelTarifs.find({'metadata.companyId': id})
});

Logos.allow({
	insert: function(userId, file){
		var company = Companies.findOne({userId: userId});
		if (company && (file.metadata.companyId == company._id)){
			return true;
		}
		return false
	},

	read: function(){
		return true;
	},

	write: function(userId, file){
		var company = Companies.findOne({userId: userId});
		if (company && (file.metadata.companyId == company._id)){
			return true;
		}
		return false
	},

	remove: function(userId, file){
		var company = Companies.findOne({userId: userId});
		if (company && (file.metadata.companyId == company._id)){
			return true;
		}
		return false
	}
})

PdfTarifs.allow({
	insert: function(userId, file){
		var company = Companies.findOne({userId: userId});
		if (company && (file.metadata.companyId == company._id)){
			return true;
		}
		return false
	},

	read: function(){
		return true;
	},

	write: function(userId, file){
		var company = Companies.findOne({userId: userId});
		if (company && (file.metadata.companyId == company._id)){
			return true;
		}
		return false
	},

	remove: function(userId, file){
		var company = Companies.findOne({userId: userId});
		if (company && (file.metadata.companyId == company._id)){
			return true;
		}
		return false
	}
})

ExelTarifs.allow({
	insert: function(userId, file){
		var company = Companies.findOne({userId: userId});
		if (company && (file.metadata.companyId == company._id)){
			return true;
		}
		return false
	},

	read: function(){
		return true;
	},

	write: function(userId, file){
		var company = Companies.findOne({userId: userId});
		if (company && (file.metadata.companyId == company._id)){
			return true;
		}
		return false
	},

	remove: function(userId, file){
		var company = Companies.findOne({userId: userId});
		if (company && (file.metadata.companyId == company._id)){
			return true;
		}
		return false
	}
})