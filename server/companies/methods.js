Meteor.methods({
  'companies.insert': function(doc, logo){
    var userId = this.userId;
    if(Companies.find({userId:userId}).fetch().length > 0){
      throw new Meteor.Error('420','Only one company per user allowed');
    }
    doc.userId = userId;
    doc.createdAt = new Date();
    return Companies.insert(doc);
  },

  'companies.update': function(doc){
    var userId = this.userId;
    var id = Companies.findOne({userId: userId})._id
    Companies.update({_id: id}, {$set:doc});
    return id;
  }
});
