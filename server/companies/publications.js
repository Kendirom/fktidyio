Meteor.publish('company', function(id){
  return Companies.find({_id: id});
});

Meteor.publish('companiesSearch', function(query){
  var nameRegex = new RegExp(query);
  return Companies.find({name:{$regex:nameRegex, $options:'i'}});
});

Meteor.publish('userCompany', function(){
	var userId = this.userId;
	return Companies.find({userId: userId});
})
