Meteor.methods({
	'createTokens': function(emails){
		if(!this.userId){
			throw new Meteor.Error('123', 'Not logged in');
		}
		_.each(emails, function(mail){
			Tokens.insert({
				email: mail.value,
				createdDate: new Date(),
				expired: false
			}, function(err, _id){
				if(err){
					return err
				} else {
					Meteor.call('sendInvintation', _id);
				}
			})
		})
	},

	'expireToken': function(token){
		Tokens.update({_id: token}, {$set:{
			expired: true
		}})
	}

})