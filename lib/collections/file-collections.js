Logos = new FileCollection('Logos', {
	resumable: true,
	resumableIndexName: 'logo',  
	http: [ {
		method: 'get', 
		path: '/md5/:md5', 
		lookup: function(params, query) {
			return {
				md5: params.md5 
			}
		}
	}]
});

PdfTarifs = new FileCollection('PdfTarifs', {
	resumable: true,
	resumableIndexName: 'pdfTarif',  
	http: [ {
		method: 'get', 
		path: '/md5/:md5', 
		lookup: function(params, query) {
			return {
				md5: params.md5 
			}
		}
	}]
});

ExelTarifs = new FileCollection('ExelTarifs', {
	resumable: true,
	resumableIndexName: 'exelTarif',  
	http: [ {
		method: 'get', 
		path: '/md5/:md5', 
		lookup: function(params, query) {
			return {
				md5: params.md5 
			}
		}
	}]
});