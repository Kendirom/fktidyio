if(Meteor.isClient){
	Tracker.autorun(function () {
		if (!Roles.subscription.ready()) return;
		if (!(Meteor.loggingIn() || Meteor.userId())) return;
		
		var hasNoRole = Roles.userIsInRole(Meteor.userId(), 'without-role');
	    if(hasNoRole) {
	        FlowRouter.go('roleSelect');
	    }

	});
}