Template.signIn.events({
	'submit #sign-in-form' : function(e,tmpl) {
		e.preventDefault();

		var email = e.currentTarget.email.value,
			password = e.currentTarget.password.value;

		Meteor.loginWithPassword(email, password, function(error){
			if(error){
				console.log(error);
				toastr.error(error.reason);
				return;
			}
			FlowRouter.go('home');
		});

	}
});