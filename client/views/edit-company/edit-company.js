import CompanySectors from '/imports/CompanySectors.js';

Template.editCompany.onCreated(function(){
	this.subscribe('userCompany');
	this.subscribe('logo', Companies.findOne({userId: Meteor.userId()})._id);
	this.subscribe('pdf', Companies.findOne({userId: Meteor.userId()})._id);
	this.subscribe('exel', Companies.findOne({userId: Meteor.userId()})._id);
})

Template.editCompany.onRendered(function(){
	var self = this;
	this.autorun(function(){
		Logos.find().fetch();
		Companies.find().fetch();
		Tracker.afterFlush(function(){
			Logos.resumable.assignBrowse($(".logo-input"));
			PdfTarifs.resumable.assignBrowse($(".pdf-input"));
			ExelTarifs.resumable.assignBrowse($(".exel-input"));

			Logos.resumable.on('fileAdded', function (file) {
		      // Create a new file in the file collection to upload
		      self.companyLogo = file;
		      return;
		    });

		    PdfTarifs.resumable.on('fileAdded', function (file) {
		      // Create a new file in the file collection to upload
		      self.companyPdf = file;
		      return;
		    });

		    ExelTarifs.resumable.on('fileAdded', function (file) {
		      // Create a new file in the file collection to upload
		      self.companyExel = file;
		      return;
		    });

			var company = Companies.findOne({userId: Meteor.userId()});
			this.$("select").select2();
			this.$("select").val(company.sectors).change();
			$('[name=name]').val(company.name);
			$('[name=nif]').val(company.nif);
			$('[name=agentName]').val(company.agentName);
			$('[name=agentPhone]').val(company.agentPhone);
			$('[name=agentEmail]').val(company.agentEmail);
			$('[name=schedule]').val(company.schedule);
			$('[name=zip]').val(company.zip);
			$('#'+company.companyType).prop('checked', true);

		})
	})
})

Template.editCompany.helpers({
	link: function(){
		var logo = Logos.findOne({'metadata.companyId': Companies.findOne({userId: Meteor.userId()})._id})
		if(logo){
			return Logos.baseURL + "/md5/" + logo.md5
		}
	},

	sectors : function(){
		return CompanySectors;
	}
})

Template.editCompany.events({
	'submit #company-form': function(e, tmpl){
		e.preventDefault();
		var form = e.currentTarget;
		var name = form.name.value,
			nif = form.nif.value,
			sectors = $('select').val(),
			agentName = form.agentName.value,
			agentPhone = form.agentPhone.value,
			agentEmail = form.agentEmail.value,
			schedule = form.schedule.value,
			zip = form.zip.value,
			companyType = $('[name="companyType"]:checked').val();

		var companyDoc = {
			name: name,
			nif: nif,
			sectors: sectors,
			agentName: agentName,
			agentPhone: agentPhone,
			agentEmail: agentEmail,
			schedule: schedule,
			zip: zip,
			companyType: companyType
		}

		var fileInsert = tmpl.companyLogo;
		var pdfInsert = tmpl.companyPdf;
		var exelInsert = tmpl.companyExel;

		var imageTypeRegex = new RegExp('^image\/');
		var pdfRegex = new RegExp('^application/pdf');
		var exelRegex = new RegExp('^application/vnd');

		if(fileInsert){
			if(fileInsert.file.size>1024*1000*10 || !imageTypeRegex.test(fileInsert.file.type)){
				toastr.error('File must be an image less then 10 MB');
				return;
			} 
		}

		if(pdfInsert){
			if(pdfInsert.file.size>1024*1000*10 || !pdfRegex.test(pdfInsert.file.type)){
				toastr.error('File must be a pdf less then 10 MB');
				return;
			} 
		}

		if(exelInsert){
			if(exelInsert.file.size>1024*1000*10 || !exelRegex.test(exelInsert.file.type)){
				toastr.error('File must be an exel file less then 10 MB');
				return;
			} 
		}

		Meteor.call('companies.update', companyDoc, function(error, id){
			if(error){
				console.log(error);
				toastr.error(error.reason);
				return;
			}
			/////////////////////////////////////////////////////////////////////////
					//=================SHITCODE ALERT==================//
					////////////////////////////////////////////////////
			//=============== LOGOS INSERT =========================================//
			if(Logos.findOne({'metadata.companyId': Companies.findOne({userId: Meteor.userId()})._id}) && fileInsert){
				Logos.remove(Logos.findOne({'metadata.companyId': Companies.findOne({userId: Meteor.userId()})._id})._id);
			}
			if(fileInsert){
				Logos.insert({
			        _id: fileInsert.uniqueIdentifier,  // This is the ID resumable will use
			        filename: fileInsert.fileName,
			        contentType: fileInsert.file.type,
			        metadata: {
			        	companyId: id
			        }
			      }, function (err, _id) {  // Callback to .insert
			        if (err) { return console.error("File creation failed!", err); }
			        // Once the file exists on the server, start uploading
			        Logos.resumable.upload();
			      }
			    );
			}
			//=============== PDF INSERT =========================================//
			if(PdfTarifs.findOne({'metadata.companyId': Companies.findOne({userId: Meteor.userId()})._id}) && pdfInsert){
				PdfTarifs.remove(PdfTarifs.findOne({'metadata.companyId': Companies.findOne({userId: Meteor.userId()})._id})._id);
			}
			if(pdfInsert){
				PdfTarifs.insert({
			        _id: pdfInsert.uniqueIdentifier,  // This is the ID resumable will use
			        filename: pdfInsert.fileName,
			        contentType: pdfInsert.file.type,
			        metadata: {
			        	companyId: id
			        }
			      }, function (err, _id) {  // Callback to .insert
			        if (err) { return console.error("Pdf creation failed!", err); }
			        // Once the file exists on the server, start uploading
			        PdfTarifs.resumable.upload();
			      }
			    );
			}
			//=============== EXEL INSERT =========================================//
			if(ExelTarifs.findOne({'metadata.companyId': Companies.findOne({userId: Meteor.userId()})._id}) && exelInsert){
				ExelTarifs.remove(ExelTarifs.findOne({'metadata.companyId': Companies.findOne({userId: Meteor.userId()})._id})._id);
			}
			if(exelInsert){
				ExelTarifs.insert({
			        _id: exelInsert.uniqueIdentifier,  // This is the ID resumable will use
			        filename: exelInsert.fileName,
			        contentType: exelInsert.file.type,
			        metadata: {
			        	companyId: id
			        }
			      }, function (err, _id) {  // Callback to .insert
			        if (err) { return console.error("Exel creation failed!", err); }
			        // Once the file exists on the server, start uploading
			        ExelTarifs.resumable.upload();
			      }
			    );
			}
			//==========================================================================//
			toastr.success('Successfully updated');
			FlowRouter.go('/company/'+id);
		});
	}
})