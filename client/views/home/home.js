Template.home.onCreated(function(){

})

Template.home.onRendered(function(){
	$('#emails-input').tokenfield({
		delimiter: ' ',
		inputType: 'email'
	});

	$('#emails-input').on('tokenfield:createtoken', function(e){
		var regExp = /\S+@\S+\.\S+/
    	var valid = regExp.test(e.attrs.value)
    	if (!valid) {
    		toastr.error('Must be an eamil')
	      	return false
	    }
	});
})

Template.home.helpers({

})

Template.home.events({
	'submit #sendEmailsForm': function(e, tmpl){
		e.preventDefault();
		var emails = $('#emails-input').tokenfield('getTokens');
		if(emails.length > 0){
			Meteor.call('createTokens', emails);
		}
	}
})