Template.companyPage.onCreated(function(){
  var companyId = FlowRouter.getParam("id");
  this.subscribe('company', companyId);
  this.subscribe('logo', companyId);
  this.subscribe('pdf', companyId);
  this.subscribe('exel', companyId);
});

Template.companyPage.onRendered(function(){

});

Template.companyPage.helpers({
  company: function(){
    return Companies.findOne({_id:FlowRouter.getParam("id")});
  },

  link: function(){
  	var logo = Logos.findOne({'metadata.companyId': FlowRouter.getParam("id")})
  	if(logo){
      return Logos.baseURL + "/md5/" + logo.md5
    }
  },

  pdfLink: function(){
    var pdf = PdfTarifs.findOne({'metadata.companyId': FlowRouter.getParam("id")})
    if(pdf){
      return PdfTarifs.baseURL + "/md5/" + pdf.md5
    }
  },

  exelLink: function(){
    var exel = ExelTarifs.findOne({'metadata.companyId': FlowRouter.getParam("id")})
    if(exel){
      return ExelTarifs.baseURL + "/md5/" + exel.md5
    }
  },

  isCompanyOwner: function(){
    return this.userId == Meteor.userId();
  }
});

Template.companyPage.events({

})
