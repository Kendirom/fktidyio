import CompanySectors from '/imports/CompanySectors.js';

Template.companyForm.onRendered(function(){
	this.$("select").select2();
	var self = this;

	Logos.resumable.assignBrowse($(".logo-input"));

	Logos.resumable.on('fileAdded', function (file) {
      // Create a new file in the file collection to upload
      self.companyLogo = file;
      return;
    });
});

Template.companyForm.helpers({
	sectors : function(){
		return CompanySectors;
	}
});

Template.companyForm.events({
	'submit #company-form': function(e, tmpl){
		e.preventDefault();
		var form = e.currentTarget;
		var name = form.name.value,
			nif = form.nif.value,
			sectors = $('select').val(),
			agentName = form.agentName.value,
			agentPhone = form.agentPhone.value,
			agentEmail = form.agentEmail.value,
			schedule = form.schedule.value,
			zip = form.zip.value,
			companyType = $('[name="companyType"]:checked').val();

		var companyDoc = {
			name: name,
			nif: nif,
			sectors: sectors,
			agentName: agentName,
			agentPhone: agentPhone,
			agentEmail: agentEmail,
			schedule: schedule,
			zip: zip,
			companyType: companyType
		}

		var fileInsert = tmpl.companyLogo;
		var imageTypeRegex = new RegExp('^image\/');

		if(fileInsert){
			if(fileInsert.file.size>1024*1000*10 || !imageTypeRegex.test(fileInsert.file.type)){
				toastr.error('File must be and image less then 10 MB');
				return;
			} 
		}

		Meteor.call('companies.insert', companyDoc, function(error, id){
			if(error){
				console.log(error);
				toastr.error(error.reason);
				return;
			}
			if(fileInsert){
				Logos.insert({
			        _id: fileInsert.uniqueIdentifier,  // This is the ID resumable will use
			        filename: fileInsert.fileName,
			        contentType: fileInsert.file.type,
			        metadata: {
			        	companyId: id
			        }
			      }, function (err, _id) {  // Callback to .insert
			      	console.log(_id);
			        if (err) { return console.error("File creation failed!", err); }
			        // Once the file exists on the server, start uploading
			        Logos.resumable.upload();
			      }
			    );
			}

			toastr.success('Successfully registered');
			FlowRouter.go('/');
		});
	}
})
