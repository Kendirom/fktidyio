Template.userForm.onCreated(function(){
	if(FlowRouter.current().route.name == 'register'){
		this.subscribe('token', FlowRouter.current().params.token);
	}
	
})

Template.userForm.onRendered(function(){
	if(FlowRouter.current().route.name == 'editProfile'){
		var user = Meteor.user();
		$('[name=firstName]').val(user.profile.firstName);
		$('[name=lastName]').val(user.profile.lastName);
		$('[name=company]').val(user.profile.company);
		$('[name=jobTitle]').val(user.profile.jobTitle);
		$('[name=email]').val(user.profile.email);
		$('[name=phone]').val(user.profile.phone);
		$('[name=description]').val(user.profile.description);
	}

	if(FlowRouter.current().route.name == 'register'){
		this.autorun(function(){
			var token = Tokens.findOne({_id:FlowRouter.current().params.token});
			Tracker.afterFlush(function(){
				if(token){
					$('[name=email]').val(token.email);
				}
			})
		})
	}
})

Template.userForm.helpers({
	isEditProfile: function(){
		return FlowRouter.current().route.name == 'editProfile';
	},
	isTokenValid: function(){
		let token = Tokens.findOne({_id:FlowRouter.current().params.token});

		return (token && (moment().diff(token.createdDate) < 24*60*60*1000) && !token.expired)  || (FlowRouter.current().route.name == 'editProfile')
	}
})

Template.userForm.events({
	'submit #user-form' : function(e, tmpl){
		e.preventDefault();

		var form = e.currentTarget;
		var firstName = form.firstName.value,
			lastName = form.lastName.value,
			company = form.company.value,
			jobTitle = form.jobTitle.value,
			email = form.email.value,
			phone = form.phone.value,
			description = form.description.value;
			if(FlowRouter.current().route.name != 'editProfile'){
				var password = form.password.value;
			}

		var profile = {
			firstName : firstName,
			lastName : lastName,
			company : company,
			jobTitle : jobTitle,
			email : email,
			phone : phone,
			description : description
		}

		if(FlowRouter.current().route.name == 'editProfile'){
			Meteor.call('users.update', profile, function(error){
				if(error){
					console.log(error);
					toastr.error(error.reason);
					return;
				}
				toastr.success('Profile edited');
				FlowRouter.go('/');
			});
		}else {
			Meteor.call('users.insert', email, password, profile, function(error){
				if(error){
					console.log(error);
					toastr.error(error.reason);
					return;
				}
				toastr.success('Successfully registered');
				Meteor.call('expireToken', FlowRouter.current().params.token);
				FlowRouter.go('signIn');
			});
		}
	}
});
