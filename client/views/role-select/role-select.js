import userRoles from '/imports/UserRoles.js';

Template.roleSelect.helpers({
	userRoles : function(){
		return userRoles;
	}
});

Template.roleSelect.events({
	'click .role-tile' : function(e, tmpl){
		e.preventDefault();

		var role = this.name,
			userId = Meteor.userId();

		Meteor.call('users.setRole', userId, role, function(error, success){
			if(error){
				console.log(error);
				toastr.error(error.reason);
				return;
			}
			toastr.success('Role was successfully set');
			FlowRouter.go('home');
		});
	}
});