Template.header.onCreated(function(){
	var self = this;
	this.searchQuery = new ReactiveVar('');

	this.subscribe('userCompany');
	this.autorun(function(){
		var query = self.searchQuery.get();
		if(query.trim().length >0){
			self.subscribe('companiesSearch', query);
		}
	})
})

Template.header.onRendered(function () {
	this.$('[data-toggle="dropdown"]').dropdown();
});

Template.header.helpers({
	userProfile : function(){
		if(Meteor.user()){
			return Meteor.user().profile;
		}
	},

	companies: function(){
		var query = Template.instance().searchQuery.get()
		var regex = new RegExp(query);
		if (query.trim().length > 0){
			return Companies.find({name:{$regex:regex, $options:'i'}})
		}
	},

	hasCompany: function(){
		return Companies.findOne({userId: Meteor.userId()});
	},

	companyId: function(){
		return Companies.findOne({userId: Meteor.userId()})._id;
	}
});

Template.header.events({
	'input .search-input': function(e, tmpl){
		var query = e.currentTarget.value;
		tmpl.searchQuery.set(query);
	},

	'click .list-group-item': function(e,tmpl){
		$('.search-input').val('');
		tmpl.searchQuery.set('');
	},

	'click .logout': function(e, tmpl){
		Meteor.logout();
		FlowRouter.go('/sign-in')
	}
})
