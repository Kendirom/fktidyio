import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';


var unsecured = FlowRouter.group();

unsecured.route('/sign-in', {
    name : 'signIn',
    action : function() {
        BlazeLayout.render("authLayout", {
            content: "signIn"
        });
    }
});

unsecured.route('/register/:token', {
    name : 'register',
    action : function() {
        BlazeLayout.render("authLayout", {
            content: "userForm"
        });
    }
});

