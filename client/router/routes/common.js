import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import UserRoles from '/imports/UserRoles.js';

var loggedIn = FlowRouter.group({
    triggersEnter: [
        function() {
            if (!(Meteor.loggingIn() || Meteor.userId())) {
                FlowRouter.go('signIn');
            }
        }
    ]
});

loggedIn.route('/', {
    name: 'home',
    action: function(){
        BlazeLayout.render("mainLayout", {
            content: "home"
        });
    }
});

loggedIn.route('/logout', {
    name: 'logout',
    action: function(){
        Meteor.logout(function(){
            FlowRouter.go('signIn')
        });
    }
});

loggedIn.route('/role-select', {
    name: 'roleSelect',
    action: function(){
        BlazeLayout.render("authLayout", {
            content: "roleSelect"
        });
    }
});

loggedIn.route('/new-company', {
    name: 'newCompany',
    action: function(){
        BlazeLayout.render("authLayout", {
            content: "companyForm"
        });
    }
});

loggedIn.route('/company/:id',{
  name: 'companyPage',
  action: function(){
    BlazeLayout.render('mainLayout',{
      content: 'companyPage'
    })
  }
});

loggedIn.route('/edit-company', {
  name: 'editCompany',
  action: function(){
    BlazeLayout.render('mainLayout',{
      content: 'editCompany'
    })
  }
})

loggedIn.route('/edit-profile', {
  name: 'editProfile',
  action: function(){
    BlazeLayout.render('mainLayout', {
      content: 'userForm'
    })
  }
});

loggedIn.route('/faq', {
  name: 'faq',
  action: function(){
    BlazeLayout.render('mainLayout', {
      content: 'faq'
    })
  }
});

loggedIn.route('/contact', {
  name: 'contact',
  action: function(){
    BlazeLayout.render('mainLayout', {
      content: 'contact'
    })
  }
});