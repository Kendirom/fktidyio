import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

FlowRouter.notFound = {
    action: function(params, queryParams) {
        BlazeLayout.render("notFound");
    }
};
